Système d'exploitation
==

**Sujet:** 

Notion de systéme d'exploitation et premières manipulations  des commandes en ligne du shell. 

**Points du programme:**

	- Identifier les fonctions d'un systéme d'exploitation
	- Utiliser les commandes de base en ligne de commande.

**Pré requis:**

Modéle d'architecture séquentielle. 

**Préparation:**

Utilisation d'une salle informatique avec des pc sous ubuntu et ayant un interpréteur de commande en ligne type tilix.

**Eléments de cours:**

	1. Systéme d'exploitation. 
		- définition 
		- utilité
		- systémes d'exploitations connus.
	2. Shell
		- services offerts par le shell
		- différentes types de shell: 
			Bureau graphique
			Ecran d'accueil du smartphone
			Interpréteur de commandes en ligne
	3. Les commandes de base en ligne de commande. 
		explication des différentes commandes en ligne de base,syntaxe générale, notion d'options. 
	

**Séance pratique:**
L'idée est de travailler en paralléle sur les commandes en ligne et sur l'interface graphique pour aller observer l'action des commandes en ligne sur les répertoires et les dossiers. 

1. Dans le gestionnaire de fichiers, sur le bureau graphique

	- dans votre espace de travail, créer un dossier que vous appelerez TP. 
	- Dans ce dossier, créer deux sous dossiers que vous appelerez partie_1 et partie_2
	- créer, en utilisant le bloc note, un document texte que vous appelerez travail_1. Vous le mettrez dans le dossier partie_1

2. Dans l'interpréteur de commandes en ligne.
	
	Je suppose que les élèves sont bien placés sur leur espace de travail.
	- Effectuer la commande `ls`. 
			
		Que semble-t-elle faire? 
		
		En utilisant l'interface graphique, créer un dossier Essai dans votre espace de travail. 
		Effectuer à nouveau la commande `ls`. Que pouvez vous en conclure? 

	- La commande `cd` permet de changer de répertoire (cd signifie change directory)
		
		On peut l'utiliser de différentes manières. 
		
		On utilise:

			-  soit un lien absolu: cd home/utilisateurs/TP/partie_1
			-  soit un lien relatif: cd partie_1
		Essayer les deux chemins et vérifier que vous êtes bien dans le dossier partie_1 en utilisant la commande `ls`. 

	- la commande `mkdir` permet de créer un nouveau dossier. 
		
		Placer vous dans le dossier partie_2 et créer un sous dossier travail. Utiliser l'interface graphique pour vérifier que le dossier est bien créé. 

	- la commande `mv` permet de déplacer ou de renommer un dossier.
		Placer vous dans le dossier TP. 
		- `mv partie_1 premiere_partie` renomme le dossier partie_1. 
		- `mv premiere_partie ~/partie_2/travail` déplace le contenu de premiere_partie au dossier travail. 
		Effectuer ces manipulations et vérifier que cela fonctionne. 

	- la commande `cp` (copy) effectue une copie d'un fichier ou d'un répertoire. 
		
		Elle s'utilise de la même manière que la commande mv, mais laisse le fichier source en place. 
		Effectuer une copie de travail dans le dossier premiere_partie.
	- la commande `rm` supprime un dossier ou un fichier. 
		Supprimer le fichier travail_1. 

	- la commande `man` permet de connaitre les options d'une commande. 
		chercher les  options de la commande mkdir. 




**QCM EC3:** 

1. Parmi les noms suivants, qui n'est pas un systéme d'exploitation:
	a. Androïd	

	b. Apple
	
	c. windows	

	d. GNU/Linux

2. Pour copier un repertoire en conservant le contenu à la source, il faut utiliser la commande:
	
	a. `cp`		

	b. `mkdir`	

	c. `mv`		

	d. `ls`
